package com.example.mvvmapp.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.mvvmapp.data.respository.UserRepositoryImpl
import com.example.mvvmapp.data.storage.SharedPrefUserStorage
import com.example.mvvmapp.data.storage.UserStorage
import com.example.mvvmapp.databinding.ActivityMainBinding
import com.example.mvvmapp.domain.models.SaveUserNameParam
import com.example.mvvmapp.domain.models.UserName
import com.example.mvvmapp.domain.usecase.GetUserNameUseCase
import com.example.mvvmapp.domain.usecase.SaveUserNameUseCase

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var vm: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        vm = ViewModelProvider(this, MainViewModelFactory(this)).get(MainViewModel::class.java)
        vm.resultLiveData.observe(this, {
            binding.dataTextView.text=it
        })
        binding.receiveButton.setOnClickListener {
            val result_load = vm.load()
            //binding.dataTextView.text = result_load
        }

        binding.sendButton.setOnClickListener {
            val text = binding.dataEditText.text.toString()
            //binding.dataTextView.text = vm.save(text)
        }
    }
}