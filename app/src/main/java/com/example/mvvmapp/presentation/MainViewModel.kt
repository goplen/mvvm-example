package com.example.mvvmapp.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvmapp.data.respository.UserRepositoryImpl
import com.example.mvvmapp.data.storage.SharedPrefUserStorage
import com.example.mvvmapp.domain.models.SaveUserNameParam
import com.example.mvvmapp.domain.models.UserName
import com.example.mvvmapp.domain.usecase.GetUserNameUseCase
import com.example.mvvmapp.domain.usecase.SaveUserNameUseCase

class MainViewModel(
    private val saveUserNameUseCase: SaveUserNameUseCase,
    private val getUserNameUseCase: GetUserNameUseCase
): ViewModel() {
    private val resultLive = MutableLiveData<String>()
    val resultLiveData: LiveData<String> = resultLive
//    private val userRepository by lazy(LazyThreadSafetyMode.NONE) {  UserRepositoryImpl(
//        SharedPrefUserStorage(context = applicationContext)
//    ) } // Lazy by инициализация только тогда когда нужен метод
//    private val getUserNameUseCase by lazy(LazyThreadSafetyMode.NONE) { GetUserNameUseCase(userRepository) }
//    private val saveUserNameUseCase by lazy(LazyThreadSafetyMode.NONE) { SaveUserNameUseCase(userRepository) }

    init {
        Log.d("mvvm", "created")
    }

    override fun onCleared() {
        Log.d("mvvm", "deleted")
        super.onCleared()
    }

    fun save(text: String) {
        val params = SaveUserNameParam(name =text)
        val result: Boolean = saveUserNameUseCase.execute(param = params)
        resultLive.value = "Save result = $result"
    }

    fun load() {
        val userName: UserName = getUserNameUseCase.execute() // Получение введенных данных
        resultLive.value = userName.firstName
    }
}