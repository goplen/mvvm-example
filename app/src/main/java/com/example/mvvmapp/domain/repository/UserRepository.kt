package com.example.mvvmapp.domain.repository

import com.example.mvvmapp.domain.models.SaveUserNameParam
import com.example.mvvmapp.domain.models.UserName

interface UserRepository { // Создание интерфейса для методов
    fun saveName(saveparam: SaveUserNameParam): Boolean // Метод сохранения данных

    fun getName(): UserName // Метод получения данных
}