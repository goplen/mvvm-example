package com.example.mvvmapp.domain.usecase

import com.example.mvvmapp.domain.models.UserName
import com.example.mvvmapp.domain.repository.UserRepository

class GetUserNameUseCase(private val userRepository: UserRepository) { // Унаследуем от интерфейса
    // Все условия должны быть прописаны в useCase
    fun execute(): UserName{

        return userRepository.getName() // полчением имени пользователя
//        return UserName("Oleg", "Olotov")
    }
}